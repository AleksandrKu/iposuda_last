<?php
class ModelCatalogCategory extends Model {
	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
    /* ******************** Manufactures *********/
    public function getCategoriesManufacturers($parent_id = 0, $manufacturer_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
        LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
        LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
         WHERE c.parent_id = '" . (int)$parent_id . "' 
         AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
         AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  
         AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
        $product_array = [];
        if ($query->num_rows) {
            $product_array = $query->rows;
            $i = 0;
            foreach ($query->rows as $category) { // второй уровень подкатегорий "Бокалы"
                $categories_3 = $this->getCategories($category['category_id']);
                $is_products = false;
                if (count($categories_3) > 0) {
                    foreach ($categories_3 as $result) { //3-й уровень в подкатегориях, которых не видно
                        $query_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category p2c 
                                     WHERE p2c.category_id = '" . (int)$result['category_id'] . "'");
                        if ($query_product->num_rows) {
                            foreach ($query_product->rows as $result2) { // все товары этой категории 3-го уровня
                                $sql = "SELECT * FROM " . DB_PREFIX . "product  
                                     WHERE product_id = '" . (int)$result2['product_id'] . "' 
                                     AND manufacturer_id = '" . (int)$manufacturer_id . "' 
                                     AND status=1";
                                $query_product_manufarturer = $this->db->query($sql);
                                if ($query_product_manufarturer->num_rows) {
                                    $is_products = true;
                                    break 2;
                                }
                            }
                        }
                    }
                } else { // если нет 3-го уровня категории
                    $query_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category p2c 
                                     WHERE p2c.category_id = '" . (int)$category['category_id'] . "'");
                    if ($query_product->num_rows) {
                        foreach ($query_product->rows as $result2) { // все товары этой категории 3-го уровня
                            $sql = "SELECT * FROM " . DB_PREFIX . "product  
                                     WHERE product_id = '" . (int)$result2['product_id'] . "' 
                                     AND manufacturer_id = '" . (int)$manufacturer_id . "' 
                                     AND status=1";
                            $query_product_manufarturer = $this->db->query($sql);
                            if ($query_product_manufarturer->num_rows) {
                                $is_products = true;
                                break;
                            }
                        }
                    }
                }
                if (!$is_products) { //если нет в этой подкатегории товара с таким брендом, удаляем из массива
                    unset($product_array[$i]);
                }
                $i++;
            }
        }
        return $product_array;
    }
    /* ******************** Manufactures *********/
	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}
}