<?php
// Heading
$_['heading_title'] = 'Мои избранные';

// Text
$_['text_account']  = 'Личный кабинет';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Избранные (%s)';
$_['text_login']    = 'Необходимо войти в <a href="%s">Личный кабинет</a> или <a href="%s">создать учетную запись</a>, чтобы добавить товар <a href="%s">%s</a> в свои <a href="%s">избранные</a>!';
$_['text_success']  = 'Товар <a href="%s">%s</a> успешно добавлен в <a href="%s">избранные</a>!';
$_['text_remove']   = 'Список избранных успешно обновлен!';
$_['text_empty']    = 'Список избранных пустой';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Наименование товара';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'На складе';
$_['column_price']  = 'Цена';
$_['column_action'] = 'Действие';
