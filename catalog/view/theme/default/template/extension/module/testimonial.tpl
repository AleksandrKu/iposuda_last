                <!-- testimonials -->
                <section class="section-testimonials">
<?php if($heading_title){ ?>
                    <header class="section-heading">
                        <h2><?php echo $heading_title; ?></h2>
                        <a href="<?php echo $keyword; ?>"><?php echo $button_all_text; ?></a>
                    </header>
<?php } ?>
<ul class="list-testimonials clearfix">
    <?php foreach ($reviews as $review) { ?>
                        <li>
                            <div class="text-holder">
                                <div class="meta-info">
                                    <strong class="author"><?php echo $review['author']; ?></strong>
                                    <span class="place"></span>
                                </div>
                                <p><?php echo $review['text']; ?></p>
                            </div>
                            <div class="bottom-row">
                    <?php if ($review['rating'] > 0) { ?>
                                <span class="stars stars<?php echo $review['rating'];?>"></span>
                    <?php } ?>
                                <span class="date"><?php echo $review['date_added']; ?></span>
                            </div>
                        </li>
    <?php } ?>
</ul></section>
