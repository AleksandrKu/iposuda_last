<div class="section-partners">
   <ul class="list-partners">
  <?php foreach ($manufacturers as $manufacturer) { ?>
      <li><span class="center">
      <?php if ($manufacturer['href']) { ?>
      <a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-responsive" /></a>
    <?php } ?>
      </span>
      </li>
  <?php } ?>
  </ul>
</div>

