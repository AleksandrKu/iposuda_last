<?php if ($discounts['discount']>0) { ?>
<div>
    <table class="table table-bordered discount-total">
        <tr><td><strong><?php echo $entry_orders_total; ?></strong></td><td style="white-space:nowrap;"><?php echo $discounts['total_orders']; ?></td></tr>
        <tr><td><strong><?php echo $entry_discount; ?></strong></td><td style="white-space:nowrap;"><?php echo $discounts['discount'].'%'; ?></td></tr>
        <?php if ($discounts['next']!='') { ?>
            <tr><td><strong><?php echo $entry_next_level; ?></strong></td><td style="white-space:nowrap;"><?php echo $discounts['next']; ?></td></tr>
        <?php } ?>
    </table>
</div>
<?php } ?>