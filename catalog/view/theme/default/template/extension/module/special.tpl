            <div class="row">
                <!-- products -->
                <section class="section-products">
                    <header class="section-heading">
                        <h2><?php echo $heading_title; ?></h2>
                        <a href="<?php echo $special_url; ?>">Показать все товары</a>
                    </header>
                                <ul class="list-products clearfix">
  <?php foreach ($products as $product) { ?>
<li>
                                        <div class="box" id="ptoduct<?php echo $product['product_id']; ?>">
                  <?php if ($product['special']) { ?>
                                            <span class="tag discount">Скидка</span>
                  <?php } ?>
                                            <div class="visual">
												<a href="<?php echo $product['href']; ?>">
<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
</a>
                                            </div>
                <p class="product_name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
            <?php if ($product['options']) { ?>
            <?php foreach ($product['options'] as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php $n = 1; foreach ($option['product_option_value'] as $i=>$option_value) { ?>
                               <?php if ($option_value['status']==1) { ?><?php if ($option_value['see'] == 1) { ?>
                <div class="radio row-choose clearfix <?php if ($n != 1) { ?> hidden <?php } ?>">
                    <input id="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>" <?php if ($n ==1) { ?> checked="checked"  <?php } ?> type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" class="product-<?php echo $product["product_id"]; ?> " />
                  <label for="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>">
                                                    <span class="radio-text"><?php echo $option_value['name']; ?> <?php if ($option_value['quant']) { ?>
(<?php echo $option_value['quant']; ?>шт)
<?php } ?></span>

            <?php if ($option_value['special']) { ?>
                                  <span class="price"><span class="text-grey"><?php echo $option_value['price']; ?></span> 
								  <span class="price-new" ><strong class="sum text-red"><?php echo $option_value['special']; ?></strong></span></span>
                    <?php } else { ?>
 <span class="price"><span class="price-new" ><strong class="sum text-red"><?php echo $option_value['price']; ?></strong></span></span>
                                   
			                    <?php } ?> 
                  </label>
                </div>		
			                     
			                    <?php $n++; } ?> 
<?php } ?>
                <?php  } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
                  <input readonly class="spinner quantity_input" type="text" value="<?php echo $product['minimum']; ?>" />
                                            <div class="bottom-row clearfix">
                <a class="link-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').attr('aria-valuenow'), $(this).parent().parent().find('.rad-checked').next().attr('name'), $(this).parent().parent().find('.rad-checked').next().val() );"> <span class="hidden-xs hidden-sm hidden-md text-hidden"><?php echo $button_cart; ?></span> <span class="text-red"><?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
<?php echo $product['special']; ?>
                  <?php } ?></span></a>
                <a class="link-favorite <?php echo $product['class_button']; ?>"  data-id="<?php echo $product['product_id']; ?>" title="<?php echo $button_wishlist; ?>" <?php if (empty($product['class_button'])) { ?> onclick="wishlist.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" <?php }  ?>><?php echo $button_wishlist; ?></a>
                                            </div>
                                        </div>

                                    </li>
  <?php } ?>
</ul>
                </section>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.link-cart .text-red').each(function(){
		var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
		$(this).html(default_price_value);
	});
});

$('input[type=radio]').change(function() {
	var this_box = $(this).closest('.box');
	var this_price = parseInt($(this).closest('.row-choose').find('.price .price-new').text().replace(' ',''));
	var this_qnt = parseInt($(this_box).find( ".spinner" ).spinner( "value" ));
	
	var calc_price = $.number(this_price * this_qnt,0,',',' ');
	
	$(this_box).find('.link-cart .cur-value').text(calc_price);
});

$( ".spinner" ).on( "spin", function( event, ui ) {
	str = "";
	
	var this_box = $(this).closest('.box');
	if (ui.value > 0) {
		if ($(this_box).find('.jcf-label-active .price .price-new').length > 0) {
			var this_price = parseInt($(this_box).find('.jcf-label-active .price .price-new').text().replace(' ',''));
			var this_qnt = ui.value;
			
			var calc_price = $.number(this_price * this_qnt,0,',',' ');
			
			$(this_box).find('.link-cart .cur-value').text(calc_price);
		}
	}
} );
--></script>
