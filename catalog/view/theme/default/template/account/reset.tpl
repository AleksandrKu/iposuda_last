<?php echo $header; ?>
<div class="row main"><?php echo $content_top; ?>
    <div id="content" class=""><?php echo $content_top; ?>
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                <h1><?php echo $heading_title; ?></h1>
                <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <?php $i = 1; ?>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <?php if ($i < count($breadcrumbs)) { ?>
                                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <?php if ($i < count($breadcrumbs)) { ?>
                                </a>
                            <?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ol>
            </div>
            
				<form class="reset_pass_form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
					<div class="box-login">
					  <legend><?php echo $text_password; ?></legend>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
						<div class="col-sm-10">
						  <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
						  <?php if ($error_password) { ?>
						  <div class="text-danger"><?php echo $error_password; ?></div>
						  <?php } ?>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
						<div class="col-sm-10">
						  <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control" />
						  <?php if ($error_confirm) { ?>
						  <div class="text-danger"><?php echo $error_confirm; ?></div>
						  <?php } ?>
						</div>
					  </div>
					</div>
					<div class="buttons clearfix">
					  
					  <div class="pull-left"><button type="submit" class="button"><i class="fa fa-save"></i> <?php echo $button_continue; ?></button></div>
					  <div  class="pull-right"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
					</div>
			  </form>
		  </div>
	  </div>
</div>
<?php echo $footer; ?>
<?php /*
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_password; ?></legend>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right"><button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_continue; ?></button></div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
*/ ?>