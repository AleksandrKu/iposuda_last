<?php echo $header; ?>
<div class="row main "><?php echo $content_top; ?>
    <div class="form-catalog">
        <div class="content-holder">
            <!-- heading -->
            <div class="heading clearfix">
                <h1><?php echo $heading_title; ?></h1>
                <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <?php $i = 1; ?>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                                <?php if ($i < count($breadcrumbs)) { ?>
                                <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <?php if ($i < count($breadcrumbs)) { ?>
                                </a>
                            <?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>" />
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                </ol>
            </div>
            <?php if ($categories) { ?>
                <p><strong><?php echo $text_index; ?></strong>
                    <?php foreach ($categories as $category) { ?>
                        &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
                    <?php } ?>
                </p>
                <?php foreach ($categories as $category) { ?>
                    <h2 id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h2>
                    <?php if ($category['manufacturer']) { ?>
                        <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
                            <div class="row">
                                <?php foreach ($manufacturers as $manufacturer) { ?>
                                    <div class="col-sm-3"><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons clearfix">
                    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>
            <?php } ?>
        </div>
    </div>
    <span id="brands-seo-text"></span>
</div>
<?php echo $footer; ?>