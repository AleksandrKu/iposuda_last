<?php echo $header; ?><?php //if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="spiner__overlay">
    <div class="spinner-css">
  <span class="side sp_left">
    <span class="fill"></span>
  </span>
        <span class="side sp_right">
    <span class="fill"></span>
  </span>
    </div>
</div>
<div class="row main ">
    <?php echo $content_top; ?>
    <div class="form-catalog">
        <aside class="sidebar-catalog">
            <div class="catalog-popup">
                <?php if ($categories) { ?>
                <div class="open-close active">
                    <strong class="slide-opener">Подкатегории</strong>
                    <div class="slide-block">
                        <ul>
                            <?php foreach ($categories as $category) { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <?php } ?>

                <?php echo $column_left; ?></div>
        </aside>
        <div id="content" class="content" data-ajax-content="scroll">

            <div class="content-holder" data-ajax-content="container">
                <!-- heading -->
                <div class="heading clearfix">
                    <h1><?php echo $heading_title; ?></h1>
                    <ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <?php $i=1; ?>
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li itemprop="itemListElement" itemscope
                            itemtype="http://schema.org/ListItem">
                            <?php if($i<count($breadcrumbs)) { ?>
                            <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <?php } ?>
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <?php if($i<count($breadcrumbs)) { ?>
                            </a>
                            <?php } ?>
                            <meta itemprop="position" content="<?php echo $i ?>"/>
                        </li>
                        <?php $i++; ?>
                        <?php } ?>
                    </ol>
                </div>
                <?php if ($products) { ?>
                <div class="sort-block">

                    <span class="text"><?php echo $text_sort; ?></span>
                    <div class="popup-holder">
                        <span class="opener"><span class="inner"></span></span>
                        <div class="popup">
                            <ul>
                                <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <li class="active"><a class="close-link"
                                                      href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
                                </li>
                                <?php } else { ?>
                                <li><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                            <script type="text/javascript">
                                $(".sort-block > .popup-holder > span > span").html($(".active > a").html());
                            </script>
                        </div>
                    </div>

                </div>
                <ul class="list-products clearfix" data-load-more="container">
                    <?php foreach ($products as $product) { ?>
                    <li class="product-tile">
                        <div class="box" id="ptoduct<?php echo $product['product_id']; ?>">
                            <?php if ($product['speciales']) { ?>
                            <span class="tag discount">Скидка</span>
                            <?php } ?>
                            <div class="visual">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <div id="h2-div"><a
                                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['options']) { ?>
                            <?php foreach ($product['options'] as $option) { ?>
                            <?php if ($option['type'] == 'select') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <select name="option[<?php echo $option['product_option_id']; ?>]"
                                        id="input-option<?php echo $option['product_option_id']; ?>"
                                        class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                        <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                        )
                                        <?php } ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'radio') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php $n = 1; foreach ($option['product_option_value'] as $i=>$option_value) { ?>
                                    <?php if ($option_value['status']==1) { ?><?php if ($option_value['see'] == 1) { ?>
                                    <div class="radio row-choose clearfix <?php if ($n != 1) { ?> hidden <?php } ?>">
                                        <input id="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>" <?php if ($n ==1) { ?>
                                        checked="checked"  <?php } ?> type="radio"
                                        name="option[<?php echo $option['product_option_id']; ?>]"
                                        value="<?php echo $option_value['product_option_value_id']; ?>"
                                        class="product-<?php echo $product["product_id"]; ?> " />
                                        <label for="option-<?php echo $i.'-'.$option_value['product_option_value_id']; ?>">
                                                    <span class="radio-text"><?php echo $option_value['name']; ?> <?php if ($option_value['quant']) { ?>
                                                        (<?php echo $option_value['quant']; ?>шт)
                                                        <?php } ?></span>

                                            <?php if ($option_value['special']) { ?>
                                            <span class="price"><span
                                                        class="text-grey"><?php echo $option_value['price']; ?></span>
								  <span class="price-new"><strong
                                              class="sum text-red"><?php echo $option_value['special']; ?></strong></span></span>
                                            <?php } else { ?>
                                            <span class="price"> <span class="price-new"><strong
                                                            class="sum text-red"><?php echo $option_value['price']; ?></strong></span></span>

                                            <?php } ?>
                                        </label>
                                    </div>

                                    <?php $n++; } ?>
                                    <?php } ?>
                                    <?php  } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'checkbox') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"
                                                   name="option[<?php echo $option['product_option_id']; ?>][]"
                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                            )
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'image') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="radio">
                                        <label>
                                            <input type="radio"
                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                            <img src="<?php echo $option_value['image']; ?>"
                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                 class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>
                                            )
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'text') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                       value="<?php echo $option['value']; ?>"
                                       placeholder="<?php echo $option['name']; ?>"
                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                       class="form-control"/>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'textarea') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
                                          placeholder="<?php echo $option['name']; ?>"
                                          id="input-option<?php echo $option['product_option_id']; ?>"
                                          class="form-control"><?php echo $option['value']; ?></textarea>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'file') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>"
                                        data-loading-text="<?php echo $text_loading; ?>"
                                        class="btn btn-default btn-block"><i
                                            class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value=""
                                       id="input-option<?php echo $option['product_option_id']; ?>"/>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'date') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'datetime') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group datetime">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control"/>
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'time') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"
                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group time">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                           value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                           class="form-control product<?php echo $product[" product_id"]; ?>" />
                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>
                            <input readonly class="spinner quantity_input" role="spinbutton" type="text"
                                   value="<?php echo $product['minimum']; ?>"/>
                            <div class="bottom-row clearfix">
                                <a class="link-cart"
                                   onclick="cart.add('<?php echo $product['product_id']; ?>', $(this).parent().parent().find('.quantity_input').attr('aria-valuenow'), $(this).parent().parent().find('.rad-checked').next().attr('name'), $(this).parent().parent().find('.rad-checked').next().val() );">
                                    <span class="hidden-xs hidden-sm hidden-md text-hidden"><?php echo $button_cart; ?></span>
                                    <span class="text-red"> </span></a>
                                <a data-id="<?php echo $product['product_id']; ?>"
                                   class="link-favorite <?php echo $product['class_button']; ?>"
                                   title="<?php echo $button_wishlist; ?>" <?php if (empty($product['class_button'])) { ?>
                                onclick="wishlist.add('<?php echo $product['product_id']; ?>');" <?php } else { ?>
                                onclick="wishlist.remove('<?php echo $product['product_id']; ?>');" <?php }  ?>
                                ><?php echo $button_wishlist; ?></a>
                            </div>
                        </div>
                        <script type="text/javascript"><!--
                            --></script>
                    </li>

                    <?php } ?>
                    <?php if(isset($show_next_pagelink) && $show_next_pagelink): ?>
                    <li class="next-page-link">
                        <a href="javascript:void(0);" class="box"
                           data-load-more="btn"
                           data-load-more-find="product-tile"
                           data-load-more-href="<?php echo $show_next_pagelink; ?>">
                            <div class="center"><span class="text">Загрузить еще<br/> 24 товара</span></div>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>

                <div class="pagination">
                    <div class="lds-spinner">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <?php echo $pagination; ?>
                </div>

            </div>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <?php if ($thumb || $description) { ?>
            <section class="text-article content-holder">
                <?php if ($description) { ?>
                <div class="text-section"><?php echo $description; ?></div>
                <?php } ?>
            </section>
            <?php } ?>
        </div>
    </div>
    <script type="text/javascript"><!--
        $(document).ready(function () {
            $('.link-cart .text-red').each(function () {
                var default_price_value = $(this).closest('.box').find('.jcf-label-active .price .price-new').html();
                $(this).html(default_price_value);
            });
        });

        $('input[type=radio]').change(function () {
            var this_box = $(this).closest('.box');
            var this_price = parseInt($(this).closest('.row-choose').find('.price .price-new').text().replace(' ', ''));
            var this_qnt = parseInt($(this_box).find(".spinner").spinner("value"));

            var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

            $(this_box).find('.link-cart .cur-value').text(calc_price);
        });

        $(document).on('spin', '.spinner', function (event, ui) {

            var this_box = $(this).closest('.box');
            if (ui.value > 0) {
                if ($(this_box).find('.jcf-label-active .price .price-new').length > 0) {
                    var this_price = parseInt($(this_box).find('.jcf-label-active .price .price-new').text().replace(' ', ''));
                    var this_qnt = ui.value;

                    var calc_price = $.number(this_price * this_qnt, 0, ',', ' ');

                    $(this_box).find('.link-cart .cur-value').text(calc_price);
                }
            }
            if (ui.value <= 1) {
                jQuery(this).spinner("value", 1);
                jQuery(this).siblings('.ui-spinner-down').addClass('disabled');
                return false;
            }
        });

        $(window).load(function() {
           $('.spiner__overlay').fadeOut('slow');
        });

        --></script>
    <?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>

<?php if(isset($min_max_prices) && !empty($min_max_prices)): ?>
<?php
	$lowPrice = ceil($min_max_prices['min_price']);
	$highPrice = ceil($min_max_prices['max_price']);

	if(isset($min_max_prices['min_special_price']) && $min_max_prices['min_special_price'] && $min_max_prices['min_special_price'] < $min_max_prices['min_price']):
		$lowPrice = ceil($min_max_prices['min_special_price']);
	endif;

	if(isset($min_max_prices['max_special_price']) && $min_max_prices['max_special_price'] && $min_max_prices['max_special_price'] > $min_max_prices['max_price']):
$highPrice = ceil($min_max_prices['max_special_price']);
endif;

if($lowPrice && $highPrice): ?>
<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "Product",
			"name": "<?php echo $heading_title; ?>",
			"offers":	{
				"@type": "AggregateOffer",
				"lowPrice": "<?php echo $lowPrice; ?>",
				"highPrice": "<?php echo $highPrice; ?>",
				"priceCurrency": "UAH"
			}
		}

</script>
<?php endif; ?>
<?php endif; ?>
<?php echo $footer; ?>
