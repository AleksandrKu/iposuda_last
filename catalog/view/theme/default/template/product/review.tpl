<?php if ($reviews) { ?>
                            <ul class="testimonials-list">
<?php foreach ($reviews as $review) { ?>

 <li>
                                    <div class="meta-info">
                                        <strong class="author"><?php echo $review['author']; ?></strong>
                                    </div>
                                    <p><?php echo $review['text']; ?></p>
<?php if ($review['advantages']) { ?>
                                    <strong class="title">Достоинства</strong>
                                    <p><?php echo $review['advantages'] ?></p>
<?php } ?>
<?php if ($review['limitations']) { ?>
                                    <strong class="title">Недостатки</strong>
                                    <p><?php echo $review['limitations'] ?></p>
<?php } ?>
                                    <div class="bottom-row">
      <?php if ($review['rating'] > 0) { ?>
                                        <span class="stars stars<?php echo $review['rating']; ?>"></span>
<?php } ?>
                                        <span class="date"><?php echo $review['date_added']; ?></span>
                                    </div>
                                </li>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
                           </ul>
<?php 
$count_reviews = count($reviews);
$count_reviews_text = '';
$count_reviews_end = $count_reviews%10;
if($count_reviews_end == 1):
	$count_reviews_text = "(<span class='text-red'>".$count_reviews."</span> отзыв)";
elseif($count_reviews_end > 1 && $count_reviews_end < 5):
	$count_reviews_text = "(<span class='text-red'>".$count_reviews."</span> отзыва)";
else:
	$count_reviews_text = "(<span class='text-red'>".$count_reviews."</span> отзывов)";
endif;
 ?>
<script type="text/javascript">
$(".tab-reviews > span ").html("(<span class='text-red'><?php echo count($reviews);?></span> отзыва)");
$(".tab_drawer_heading > span ").html("(<span class='text-red'><?php echo count($reviews);?></span> отзыва)");
</script>
          
<?php } else { ?>
<?php } ?>

