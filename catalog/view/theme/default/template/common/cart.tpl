<div id="cart" class="btn-group btn-block">
    <a class="<?php if ($text_quant > 0) { ?>active <?php } ?> cart" href="" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>"><?php echo $text_quant; ?></a>
    <div class="dd-menu dropdown-menu no-close">
        <div class="dd-menu__content">
            <?php if ($products || $vouchers) { ?>
                <div class="products cart_product_list">
                    <?php foreach ($products as $product): ?>
                        <div class="products__row">
                            <div class="products__row-first">
                                <div class="products__image">
                                    <?php if ($product['thumb']) { ?>
                                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                                    <?php } ?>
                                </div>
                                <div class="products__info">
                                    <ul class="products__info-ul">
                                        <li class="products__info-name">
                                            <a href="<?php echo $product['href']; ?>">
                                                <?php echo $product['name']; ?>
                                            </a>
                                        </li>
                                        <?php if ($product['option']) { ?>
                                            <?php foreach ($product['option'] as $option) { ?>
                                                 <li class="products__info-pack"><?php echo $option['value']; ?></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="products__row-second">
                                <div class="products__count">
                                    х
                                    <?php echo $product['quantity']; ?>
                                </div>
                                <div class="products__price">
                                    <?php echo $product['price']; ?>
                                </div>
                                <div class="products__del">
                                    <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="delete"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="price">
                    <?php foreach ($totals as $total) { ?>
                        <div class="price__row">
                            <div class="price__text"><?php echo $total['title']; ?></div>
                            <div class="price__summ"><?php echo $total['text']; ?></div>
                        </div>
                    <?php } ?>

                    <div class="buttons">
                        <a class="button" href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a>
                        <a class="button" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
                    </div>
                </div>
            <?php } else { ?>
                <p class="text-center"><?php echo $text_empty; ?></p>
            <?php } ?>
        </div>
    </div>
</div>