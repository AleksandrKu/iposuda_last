<?php if ($error_warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
    <?php foreach ($payment_methods as $payment_method) { ?>
        <?php $payment_custom_class = 'hidden'; ?>
        <div class="radio">
            <label for="payment_<?php echo $payment_method['code']; ?>">
                <?php if ($payment_method['code'] == $code || !$code) { ?>
                    <?php $code = $payment_method['code']; ?>
                    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="payment_<?php echo $payment_method['code']; ?>" checked="checked" />
                    <?php $payment_custom_class = 'showing'; ?>
                <?php } else { ?>
                    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="payment_<?php echo $payment_method['code']; ?>" />
                <?php } ?>
                <?php echo $payment_method['title']; ?>
                <?php if ($payment_method['terms']) { ?>
                    (<?php echo $payment_method['terms']; ?>)
                <?php } ?>
                <?php if (isset($payment_method['description'])) { ?>
                    <br /><small><?php echo $payment_method['description']; ?></small>
                <?php } ?>
            </label>
            <?php if ($payment_method['code'] == 'cod1') { ?>
                <div class="payment_custom_field  <?php echo $payment_custom_class; ?>">
                    <input type="text" id="payment_custom_method" name="payment-custom-method" value="" placeholder="Удобный способ оплаты" class="form-control" >
                </div>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>
<script>
    $(document).ready(function () {
	jcf.customForms.replaceAll();
    });
</script>
<div class="box-inner">
    <div class="label-holder"><label for="comment">Комментарий</label></div>
    <div class="input-holder"><textarea placeholder="<?php echo $text_comments; ?>" id="comment" cols="30" rows="10" name="comment"></textarea></div>
</div>

<?php if ($text_agree) { ?>
    <div class="pull-right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
            <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
            <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        &nbsp;
    </div>
    <div class="button-right">    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="button" /></div>
<?php } else { ?>
    <div class="button-right">    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="button" /></div>
<?php } ?>
