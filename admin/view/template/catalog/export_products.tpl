<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
<div class="pull-right"><a href="<?php echo $products; ?>" data-toggle="tooltip" title="<?php echo $button_products; ?>" class="btn btn-primary"><?php echo $button_products; ?></a> <a href="<?php echo $clean; ?>" data-toggle="tooltip" title="<?php echo $button_clean; ?>" class="btn btn-danger"><?php echo $button_clean; ?></a></div>
    <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
<div>
<?php if (isset($info)) { ?>
<div><?php echo $info;?></div>
<?php } ?>
<div class="col-sm-4">
Загрузить товары <br />
<form method="post" enctype="multipart/form-data">
    <input type="file" name="files" id="file"><br>
    <input type="submit" name="submit-_files" value="Загрузить">
</form>
</div>
<div class="col-sm-8">
<table class="table table-bordered table-hover">
<thead><tr><td>Name</td></tr>
</thead>
<?php if (isset($files)) {
foreach ($files as $file) {
echo "<tr><td><input type='radio' name='file' value='" . $file['name'] . "'>" . $file['name'] . " </td>";
echo "</tr>";
//}
}}
?> 

</table> 
</div>
<input type="submit" value="Price Mango" name="import_pro" class="importpro" />
<input type="submit" value="Price Чайка" name="import_ch" class="importch" />
</div>
<div id="progress-bar">
    </div>
    <div id="prs">
    </div> 

<script type="text/javascript">
$(document).ready(function() {
    $('input.importpro').click(function(event) { //Trigger on form submit
    if ($('input[name=file]:checked').val()) {
    if (confirm("Вы подтверждаете что хотите загрузить прайс Mango?")) {
	repeat_import_pro();
    } else {
	        return false;
	    }
    } else {
	        return false;
	    }


});
    $('input.importch').click(function(event) { //Trigger on form submit
    if ($('input[name=file]:checked').val()) { //Trigger on form submit
    if (confirm("Вы подтверждаете что хотите загрузить прайс Чайка?")) {
	repeat_import_ch();
    } else {
	        return false;
	    }
    } else {
	        return false;
	    }

});
    $('input.cancel').click(function(event) { //Trigger on form submit
repeat_import_pro.abort();
});

function repeat_import_pro() {

        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : 'index.php?route=catalog/export/ajaxGetProducts&file='+encodeURIComponent($('input[name=file]:checked').val())+'&funct='+encodeURIComponent($('input[name=funct]:checked').val())+"&"+$('.options:checked').serialize()+'&token=<?php echo $token; ?>', //Your form processing file URL
        contentType: "application/json",
            dataType  : 'json',
beforeSend: function() {
						$("#progress-bar").append("I");
 },
            success   : function(json, textStatus){
						$("#progress-bar").append("I");
						if (json["success"] == "The End") {
						if (json["error_file"]) {						
							$("#prs").html("<h2>Ошибка.</h2><p>" + json['error_file'] + "</p>");
						} else {
							$("#prs").html("<h2>Импорт завершен!</h2><p>Обновлено "+ json['update'] +" товаров. Добавлено " + json['insert'] + " товаров</p>");
						}
						}
						else {
							$("#prs").html("<p>Обработано "+ json['int'] +" строк. Добавлено " + json['insert'] + " товаров. Обновлено "+ json['update'] +" товаров.</p>");
							repeat_import_pro();
						} 

					},

			complete: function( xhr, textStatus){

						if (textStatus != "success") {
							$("#progress-bar").append("I");
							repeat_import_pro();		
						}
					}
        });
}

function repeat_import_ch() {

        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : 'index.php?route=catalog/export/ajaxChProducts&file='+encodeURIComponent($('input[name=file]:checked').val())+'&funct='+encodeURIComponent($('input[name=funct]:checked').val())+"&"+$('.options:checked').serialize()+'&token=<?php echo $token; ?>', //Your form processing file URL
        contentType: "application/json",
            dataType  : 'json',
beforeSend: function() {
						$("#progress-bar").append("I");
 },
            success   : function(json, textStatus){
						$("#progress-bar").append("I");
						if (json["success"] == "The End") {
						if (json["error_file"]) {						
							$("#prs").html("<h2>Ошибка.</h2><p>" + json['error_file'] + "</p>");
						} else {
							$("#prs").html("<h2>Импорт завершен!</h2><p>Обновлено "+ json['update'] +" товаров. Добавлено " + json['insert'] + " товаров</p>");
						}
						}
						else {
							$("#prs").html("<p>Обработано "+ json['int'] +" строк. Добавлено " + json['insert'] + " товаров. Обновлено "+ json['update'] +" товаров.</p>");
							repeat_import_ch();
						} 

					},

			complete: function( xhr, textStatus){

						if (textStatus != "success") {
							$("#progress-bar").append("I");
							repeat_import_ch();		
						}
					}
        });
}

});
</script>
    </div>
  </div>
</div>
<?php echo $footer; ?>
