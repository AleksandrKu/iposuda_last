<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
<div class="pull-right"><a href="<?php echo $errors; ?>" data-toggle="tooltip" title="<?php echo $button_errors; ?>" class="btn btn-primary"><?php echo $button_errors; ?></a> <a href="<?php echo $products; ?>" data-toggle="tooltip" title="<?php echo $button_products; ?>" class="btn btn-primary"><?php echo $button_products; ?></a> <a href="<?php echo $options; ?>" data-toggle="tooltip" title="<?php echo $button_options; ?>" class="btn btn-primary"><?php echo $button_options; ?></a> <a href="<?php echo $clean; ?>" data-toggle="tooltip" title="<?php echo $button_clean; ?>" class="btn btn-danger"><?php echo $button_clean; ?></a></div>
    <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
<div class="man">
<form method="post" enctype="multipart/form-data">
    <label for="files">Filename:</label>
    <input type="file" name="files" id="file"><br>
    <input type="submit" name="submit-_files" value="Загрузить">
</form>
Загрузить производителей
<?php
$files1 = scandir($dir);
foreach ($files1 as $file) {
    if ((preg_match('/\.(xlsx)/', $file)) || (preg_match('/\.(XLSX)/', $file))) {
echo "<input type='radio' name='file' value='" . $file . "'>" . $file . "<br />";
}
}
?> 
<input type='radio' name='funct' value='update'>Добавить и обновить<br>
<input type='radio' name='funct' value='insert'>Добавить новые<br>
            <input type="submit" value="Импорт" name="import" class="import" />
</div>
<div id="progress-bar">
    </div>
    <div id="prs">

    </div> 
    <div id="prs2">

    </div> 
<script type="text/javascript">
$(document).ready(function() {
    $('input.import').click(function(event) { //Trigger on form submit
	repeat_import();
});
    $('input.importpro').click(function(event) { //Trigger on form submit
	repeat_import_pro();
});
    $('input.cancel').click(function(event) { //Trigger on form submit
repeat_import_pro.abort();
	});

function repeat_import() {
        var postForm = { //Fetch form data
            'file'     : $('input[name=file]:checked').val() //Store name fields value
        };

        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : 'index.php?route=catalog/export/ajaxGetMan&funct='+encodeURIComponent($('input[name=funct]:checked').val())+'&file='+encodeURIComponent($('input[name=file]:checked').val())+'&token=<?php echo $token; ?>', //Your form processing file URL
        contentType: "application/json",
 data: 'file='+$('input[name=file]:checked').val(),
            dataType  : 'json',
beforeSend: function() {
						$("#progress-bar").append("I");
 },
                        success   : function(json, textStatus){
						$("#progress-bar").append("I");
 console.log("отправляется");
						if (json["success"] == "The End") {
							$("#prs").html("<h2>Импорт завершен!</h2><div>Insert - "+json['insert']+"Double - "+json['double']+"</div>");
$("#prs2").html(json['htmltext'].join(" "));
						}
						else {
							$("#prs").html("<p>" + json['insert'] + "</p>");
							repeat_import();
						} 

					},
complete: function(json, xhr, textStatus){

					}

        });
}

function repeat_import_pro() {
        $.ajax({ //Process the form using $.ajax()
            type      : 'POST', //Method type
            url       : 'index.php?route=catalog/export/ajaxGetProducts&file='+encodeURIComponent($('input[name=file]:checked').val())+'&token=<?php echo $token; ?>', //Your form processing file URL
        contentType: "application/json",
 data: 'file='+$('input[name=file]:checked').val(),
            dataType  : 'json',
beforeSend: function() {
						$("#progress-bar").append("I");
 },
            success   : function(json, textStatus){
						$("#progress-bar").append("I");
						if (json["success"] == "The End") {
							$("#prs").html("<h2>Импорт завершен!</h2>");
						}
						else {
							$("#prs").html("<p>" + json['insert'] + "</p>");
							repeat_import_pro();
						} 

					},

			complete: function(xhr, textStatus){

						if (textStatus != "success") {
							$("#progress-bar").append("I");
							repeat_import_pro();		
						}
					}
        });
}
});
</script>
    </div>
  </div>
</div>      
<?php echo $footer; ?>
