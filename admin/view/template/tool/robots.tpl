<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
	  <div class="pull-right">
		<button id="save-robots-file" data-toggle="tooltip" title="Сохранить файл" class="btn btn-success"><i class="fa fa-save"></i> Сохранить файл</button>
		</div>
    </div>
  </div>
  <div class="container-fluid">
	 <div style="display: none;" class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	 <div style="display: none;" class="alert alert-success"><i class="fa fa-check-circle"></i>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Robots.txt</h3>
			</div>
			
			<div class="panel-body">
				<div class="row">
					
				</div>
				<div class="well">
					<div class="form-group" >
						<textarea rows=<?php echo $textarea_rows + 2; ?> class="form-control" name="file_content" id="file_content" placeholder="Контент файла"><?php echo !empty($file_content) ? $file_content : ''; ?></textarea>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>
<script>
$(document).on('click','#save-robots-file',function(){
	var file_content = $('textarea#file_content').val();
	
	$.ajax({
		url: 'index.php?route=tool/robots/saveFile&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: {
			file_content : file_content
		},
		beforeSend: function()
		{
			$('.alert').hide();
		},
		success: function(data)
		{
			if(data.error.length > 0)
			{
				$('.alert.alert-danger').text(data.error).show();
				
			}
			if(data.success.length)
			{
				$('.alert.alert-success').text(data.success).show();
			}
			
		}
	});
});
</script>
<?php echo $footer; ?>