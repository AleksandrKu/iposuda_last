<?php
class ModelCatalogCity extends Model {
	public function getCity() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cities");

		return $query->rows;
	}
	public function addCity($data) {
	$this->db->query("INSERT INTO " . DB_PREFIX . "cities SET city_name = '" . $this->db->escape($data['city_name']) . "', city_url = '" . $this->db->escape($data['city_url']) . "'");
		$this->cache->delete('citiesQueries');
	}
	public function deleteCity($city_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cities WHERE id = '" . (int)$city_id . "'");
		$this->cache->delete('citiesQueries');
	}
	public function enableCity($city_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "cities SET `status` = '1' WHERE id = '" . (int)$city_id . "'");
		$this->cache->delete('citiesQueries');
	}
	public function disableCity($city_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "cities SET `status` = '0' WHERE id = '" . (int)$city_id . "'");
		$this->cache->delete('citiesQueries');
	}
}
?>
