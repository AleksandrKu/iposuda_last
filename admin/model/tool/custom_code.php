<?php
class ModelToolCustomCode extends Model {
	
	public function addCustomCode($data)
	{
		$this->db->query( "INSERT INTO " . DB_PREFIX . "url_head_custom_code "
			. "SET `custom_code` = '" .$this->db->escape($data['custom_code']). "', "
			. "`site_url` = '" .$this->db->escape($data['site_url']). "' ");
					
		$this->cache->delete('customHeadCodes');
		
		return $this->db->getLastId();
	}
	
	public function editCustomCode($custom_code_id, $data)
	{
		$this->db->query("UPDATE " . DB_PREFIX . "url_head_custom_code "
			. "SET `custom_code` = '" . $this->db->escape($data['custom_code']) . "', "
			. "`site_url`  = '" . $this->db->escape($data['site_url']) . "' "
			. "WHERE `custom_code_id` = '" . (int)$custom_code_id . "'");
			
		$this->cache->delete('customHeadCodes');
	}
	
	public function getCustomCode($custom_code_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_head_custom_code "
			. "WHERE `custom_code_id` = '" . $custom_code_id . "'");
			
		return $query->row;
	}
	
	public function deleteCustomCode($custom_code_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_head_custom_code "
			. "WHERE `custom_code_id` = '" . $custom_code_id . "'");
			
		$this->cache->delete('customHeadCodes');
	}
	
	public function getCustomCodes()
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_head_custom_code ");
		
		return $query->rows;
	}
}
?>