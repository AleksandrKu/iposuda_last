<?php
// Heading
$_['heading_title']          = 'Города';

// Text
$_['text_success']           = 'Cписок городов обновлен!';
$_['text_list']              = 'Список городов';
$_['text_add']               = 'Добавление города';
$_['text_edit']              = 'Редактирование города';

// Column
$_['column_name']            = 'Название города';
$_['column_zone']            = 'Название региона';
$_['column_country']         = 'Страна';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название города';
$_['entry_zone']             = 'Название региона';
$_['entry_country']          = 'Страна';

// Error
$_['error_permission']       = 'У вас нет прав для изменения списка городов!';
$_['error_name']             = 'Название города должно быть от 3 до 128 символов!';

