<?php
class ControllerToolRedirect extends Controller {
	private $error = array();

	public function index() {
		
		$this->document->setTitle('Редиректы');
		
		$data = Array();
		
		if (isset($this->request->get['filter_from'])) {
			$filter_from = $this->request->get['filter_from'];
		} else {
			$filter_from = '';
		}
		
		if (isset($this->request->get['filter_to'])) {
			$filter_to = $this->request->get['filter_to'];
		} else {
			$filter_to= '';
		}
		
		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Редиректы',
			'href' => $this->url->link('tool/redirect', 'token=' . $this->session->data['token'] , true)
		);
		
		$data['heading_title'] = 'Редиректы';
		
		$this->load->model('tool/redirect');
		
		$limit = 50;
		
		$filter_data = Array(
			'filter_from' 	=> $filter_from,
			'filter_to' 	=> $filter_to,
			'filter_code'	=> $filter_code,
			'start'       	=> ($page - 1) * $limit,
			'limit'         => $limit
		);
		
		$redirect_total = $this->model_tool_redirect->getTotalRedirects($filter_data);
		
		$data['redirects'] = $this->model_tool_redirect->getRedirects($filter_data);
		
		$url = '';

		if (isset($this->request->get['filter_form'])) {
			$url .= '&filter_form=' . urlencode(html_entity_decode($this->request->get['filter_form'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_to'])) {
			$url .= '&filter_to=' . urlencode(html_entity_decode($this->request->get['filter_to'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
		}

		$pagination = new Pagination();
		$pagination->total = $redirect_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('tool/redirect', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		
		$data['filter_from'] = $filter_from;
		$data['filter_to'] = $filter_to;
		$data['filter_code'] = $filter_code;
		
		$data['token'] = $this->session->data['token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/redirect', $data));
	}
	
	public function get()
	{
		
	}
	
	public function add($data)
	{
		$this->load->model('tool/redirect');
		
		$json = Array();
		
		$json['redirect_id'] = $this->model_tool_redirect->addRedirect($data);
		$json['message'] = 'Добавлен';
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
		return;
	}
	
	public function edit()
	{
		if($this->request->post['redirect_id'] != 'new'):
			$this->load->model('tool/redirect');
			
			$json = Array();
			
			$redirect_id = $this->request->post['redirect_id'];
			
			$redirect_info = $this->model_tool_redirect->getRedirect($redirect_id);
			
			if($redirect_info):
				$this->model_tool_redirect->editRedirect($redirect_id, $this->request->post);
				$json['message'] = 'Сохранен';
			else:
				$json['message'] = 'Был удален';
			endif;
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		else:
			$this->add($this->request->post);
		endif;
		
		return;
	}
	
	public function remove()
	{
		$this->load->model('tool/redirect');
		
		$this->model_tool_redirect->deleteRedirect($this->request->post['redirect_id']);
		
		return;
	}
	
}
?>