<?php
class ControllerToolCustomCode extends Controller {
	public function index() {
		
		$this->load->model('tool/custom_code');
		
		$this->document->setTitle('Произвольный код');
		
		$data = Array();
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Произвольный код',
			'href' => $this->url->link('tool/custom_code', 'token=' . $this->session->data['token'] , true)
		);
		
		$data['heading_title'] = 'Произвольный код';
		
		$data['custom_codes'] = $this->model_tool_custom_code->getCustomCodes();
		
		$data['token'] = $this->session->data['token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/custom_code', $data));
	}
	
	public function edit()
	{
		if($this->request->post['custom_code_id'] != 'new'):
			$this->load->model('tool/custom_code');
			
			$json = Array();
			
			$custom_code_id = $this->request->post['custom_code_id'];
			
			$custom_code_info = $this->model_tool_custom_code->getCustomCode($custom_code_id);
			
			if($custom_code_info):
				$this->model_tool_custom_code->editCustomCode($custom_code_id, $this->request->post);
				$json['message'] = 'Сохранен';
			else:
				$json['message'] = 'Был удален';
			endif;
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		else:
			$this->add($this->request->post);
		endif;
		
		return;
	}
	
	public function add($data)
	{
		$this->load->model('tool/custom_code');
		
		$json = Array();
			
		$json['custom_code_id'] = $this->model_tool_custom_code->addCustomCode($data);
		$json['message'] = 'Добавлен';
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
		return;
	}
	
	public function remove()
	{
		$this->load->model('tool/custom_code');
		
		$this->model_tool_custom_code->deleteCustomCode($this->request->post['custom_code_id']);
		
		return;
	}
	
}