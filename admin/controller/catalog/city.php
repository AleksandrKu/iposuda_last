<?php
class ControllerCatalogCity extends Controller {
	private $error = array();
	private $category_id = 0;
	private $path = array();

	public function index() {
		$this->load->language('catalog/city');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/city');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/city');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/city');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if ((!empty($this->request->post['city_name']))&&(!empty($this->request->post['city_url']))) {
			$this->model_catalog_city->addCity($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			} else {
			$this->session->data['warning'] = "Заполните все поля!";
			}

			$this->response->redirect($this->url->link('catalog/city', 'token=' . $this->session->data['token'], true));
		}
		$this->getList();
	}

	public function action() {
		$this->load->language('catalog/city');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/city');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			switch($this->request->post['cities_action']):
				case 'enable':
					foreach ($this->request->post['selected'] as $city_id) {
						$this->model_catalog_city->enableCity($city_id);
					}
					break;
				case 'disable':
					foreach ($this->request->post['selected'] as $city_id) {
						$this->model_catalog_city->disableCity($city_id);
					}
					break;
				case 'delete':
					foreach ($this->request->post['selected'] as $city_id) {
						$this->model_catalog_city->deleteCity($city_id);
					}
					break;
			endswitch;
			

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			$this->response->redirect($this->url->link('catalog/city', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/city')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function getList() {
		$url = '';
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/category', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/city/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['action'] = $this->url->link('catalog/city/action', 'token=' . $this->session->data['token'] . $url, true);

		$this->load->model('catalog/city');

		$data['cities'] = array();

		$data['cities'] = $this->model_catalog_city->getCity();

		$city_total = count($data['cities']);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
		} else {
			$data['error_warning'] = '';

		}

			unset($this->session->data['warning']);
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/city', $data));

	}
	
}
